/*
 * It assumes the CSV has headers.
 *
 * 1. Filtering:
 *    It filters the rows where the column 'len', which stands for tooth length, is greater than a specific value.
 *
 * 2. Grouping:
 *    The program then groups the data by the 'supp' column, which stands for 'Supplement type (VC or OJ)'.
 *
 * 3. Aggregation:
 *    For each group (species in this case), it calculates the sum of the column 'do', which stands for 'Dose in milligrams/day'.
 *
 * Output Explanation:
 *    The resulting DataFrame has a shape of (2, 2), indicating that it has 2 rows and 2 columns.
 *    The rows represent the two supplement type: 'VC' and 'OJ'.
 *    The columns represent:
 *    - 'species': The name of the supplement type
 *    - 'total_dose': The sum of 'do' values for each supplement type
 *
 *    For example, for 'VC' with filtering >= 0, the sum of 'do' values is 35,
 */

// Import necessary modules from the `polars` crate
use polars::prelude::*;
use std::io::Cursor;

// This is the Tooth Growth dataset in CSV format.

const TOOTH_DATA: &str = "len,supp,dose
4.2,VC,0.5
11.5,VC,0.5
7.3,VC,0.5
5.8,VC,0.5
6.4,VC,0.5
10,VC,0.5
11.2,VC,0.5
11.2,VC,0.5
5.2,VC,0.5
7,VC,0.5
16.5,VC,1
16.5,VC,1
15.2,VC,1
17.3,VC,1
22.5,VC,1
17.3,VC,1
13.6,VC,1
14.5,VC,1
18.8,VC,1
15.5,VC,1
23.6,VC,2
18.5,VC,2
33.9,VC,2
25.5,VC,2
26.4,VC,2
32.5,VC,2
26.7,VC,2
21.5,VC,2
23.3,VC,2
29.5,VC,2
15.2,OJ,0.5
21.5,OJ,0.5
17.6,OJ,0.5
9.7,OJ,0.5
14.5,OJ,0.5
10,OJ,0.5
8.2,OJ,0.5
9.4,OJ,0.5
16.5,OJ,0.5
9.7,OJ,0.5
19.7,OJ,1
23.3,OJ,1
23.6,OJ,1
26.4,OJ,1
20,OJ,1
25.2,OJ,1
25.8,OJ,1
21.2,OJ,1
14.5,OJ,1
27.3,OJ,1
25.5,OJ,2
26.4,OJ,2
22.4,OJ,2
24.5,OJ,2
24.8,OJ,2
30.9,OJ,2
26.4,OJ,2
27.3,OJ,2
29.4,OJ,2
23,OJ,2";

// Define the main function that returns a Result type.
// accepts a filter i.e. 5.0 type f64 and returns a DataFrame
// If everything is Ok, it returns `()`, otherwise it returns a `PolarsError`.

pub fn calculate(filter: f64) -> Result<DataFrame, PolarsError> {
    // Create a Cursor object from the TOOTH_DATA constant
    let file = Cursor::new(TOOTH_DATA);
    // Read the CSV data using CsvReader
    let df = CsvReader::new(file)
        .has_header(true)
        .finish()?
        .lazy()
        .filter(col("len").gt(lit(filter)))
        .groupby(vec![col("supp")])
        .agg(&[
            col("dose").sum().alias("total_dose"),
        ])
        .collect()?;
    Ok(df)
}
