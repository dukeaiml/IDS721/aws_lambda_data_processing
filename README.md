# AWS Lambda for Tooth Growth Dataset Processing
## Purpose of Project
The purpose of this project is to create a serverless AWS Lambda function using the Rust programming language and Cargo Lambda. The project focuses on AWS Lambda functionality, AWS API Gateway integration, and efficient data processing. This AWS lambda is built for the process of Tooth Growth Dataset, which includes these following steps:

 * 1. Filtering:  
   It filters the rows where the column 'len', which stands for tooth length, is greater than a specific value.  <br/>
 * 2. Grouping: 
   The program then groups the data by the 'supp' column, which stands for 'Supplement type (VC or OJ)'.  <br/>
 * 3. Aggregation:  
   For each group (species in this case), it calculates the sum of the column 'do', which stands for 'Dose in milligrams/day'.
 * Output Explanation:<br/>
     The resulting DataFrame has a shape of (2, 2), indicating that it has 2 rows and 2 columns.
     The rows represent the two supplement type: 'VC' and 'OJ'.
     The columns represent:  
     - 'species': The name of the supplement type
     - 'total_dose': The sum of 'do' values for each supplement type  <br/>
     - For example, for 'VC' with filtering >= 0, the sum of 'do' values is 35.

The user would just need to invoke my Lambda function by calling `make aws-invoke` (with the appropriate credentials) or send a request through the API Gateway at the link below.

## Testing and Application
* You can test your api gateway by sending a curl post request in your terminal:
```
curl -X POST "https://28ypfjcu30.execute-api.us-east-1.amazonaws.com/dev" \
     -H "Content-Type: application/json" \
     --data '{"filter": 10.0}'
```

* You can test your lambda fucntion without using the api gateway like so in your terminal:  
`cargo lambda invoke --remote aws_lambda_data_processing --data-ascii "{ \"filter\": 10.0}"`  

* You can subsitute "10.0" according to you requirement.


## Diagram of Final Lambda Diagram Integration
![Screenshot_2024-02-01_at_4.06.06_PM](https://gitlab.com/zhankai-ye/aws_lambda_IDS721_2/-/raw/656b42ae4ac31117122f5015f5b954d105810342/upload/aws1.png)

## Successful Test Example
![Screenshot_2024-02-01_at_4.17.01_PM](https://gitlab.com/zhankai-ye/aws_lambda_IDS721_2/-/raw/main/upload/aws2.png?ref_type=heads)
